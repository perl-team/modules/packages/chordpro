#!/bin/sh

echo "Chordii has been discontinued."
echo "Its successor is a newly developed yet still mostly compatible tool ChordPro."
echo "Please use ChordPro (binary: chordpro) in the future."
echo "The chordii package can now be removed."

exit 1
